import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class AirtableNotifyTestCase(PipeTestCase):

    def test_default_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": {"Company": "Airtable"}
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_double_quotes_in_message_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": '"Pipelines is awesome!"'
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_debug_flag_success(self):
        result = self.run_container(environment={
            "WEBHOOK_URL": os.getenv("WEBHOOK_URL"),
            "MESSAGE": {"Company": "Airtable"},
            "DEBUG": "true"
        })

        self.assertRegex(result, rf'✔ Notification successful.')

    def test_default_failed(self):
        BAD_WEBHOOK_URL = "https://hooks.airtable.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"

        result = self.run_container(environment={
            "WEBHOOK_URL": BAD_WEBHOOK_URL,
            "MESSAGE": {"Company": "Airtable"}
        })

        self.assertRegex(result, rf'✖ Notification failed.')
